var color = 'rgb(0,0,0)';
var lastcolor = 'rgb(0,0,0)';
var width = "2";
var brushcap = 'round';
var brushjion = 'round';
var colornow;
var ctxcolor;
var iseraser = 0;
var istexting = 0;
var isshape = 0;
var isfill = 1;
var shapex;
var shapey;
var widthtext;
var texttext;
var fontinfo;
var textsize = 1;
var textfont = 'Arial';
var cPushArray = new Array();
var cStep = -1;

function color_now(x){
    colornow = document.getElementById('colornow');
    widthtext = document.getElementById('widthtext');
    fontinfo = document.getElementsByClassName('fontinfo');
    if(iseraser&&x)alert("You are using ERASER now!");
    colornow.style.backgroundColor = color;
    if(color=='white'){
        widthtext.style.color = 'black';
        fontinfo.style.color = 'black';
    }
    else{
        widthtext.style.color = color;
        fontinfo.style.color = color;
    }

}

function mouseDown(e){
    colornow = document.getElementById('colornow');
    this.ctx = this.getContext("2d");
    this.ctx.strokeStyle = ctxcolor;
    this.ctx.lineWidth = width;
    this.ctx.lineCap = brushcap;
    this.ctx.lineJoin = brushjion;
    var textctx = this.getContext("2d");
    textctx.fillStyle = colornow.style.backgroundColor;

    this.draw=true;
    var o=this;
    this.offsetX=this.offsetLeft;
    this.offsetY=this.offsetTop;

    while(o.offsetParent){
        o=o.offsetParent;
        this.offsetX+=o.offsetLeft;
        this.offsetY+=o.offsetTop;
    }

    this.ctx.beginPath();
    shapex = e.pageX-this.offsetX;
    shapey = e.pageY-this.offsetY;
    
    if(istexting){
        textctx.fillStyle = colornow.style.backgroundColor;
        if(iseraser)alert("You are using ERASER now!");
        else{
            var textinput = document.getElementById('textinput').value;
            if(textfont=='Arial'){
                if(textsize==1)textctx.font = "20px Arial";
                else if(textsize==2)textctx.font = "24px Arial";
                else if(textsize==3)textctx.font = "28px Arial";
            }
            else if(textfont=='Lobster'){
                if(textsize==1)textctx.font = "20px Lobster";
                else if(textsize==2)textctx.font = "24px Lobster";
                else if(textsize==3)textctx.font = "28px Lobster";
            }
            else if(textfont=='Acme'){
                if(textsize==1)textctx.font = "20px Acme";
                else if(textsize==2)textctx.font = "24px Acme";
                else if(textsize==3)textctx.font = "28px Acme";
            }
            else textctx.font = "20px Arial";
            textctx.fillText(textinput, e.pageX-this.offsetX, e.pageY-this.offsetY);
        }
        
    }
    this.ctx.moveTo(e.pageX-this.offsetX,e.pageY-this.offsetY);
    
}

function mouseMove(e){
    var o=this;
    this.offsetX=this.offsetLeft;
    this.offsetY=this.offsetTop;

    while(o.offsetParent){
        o=o.offsetParent;
        this.offsetX+=o.offsetLeft;
        this.offsetY+=o.offsetTop;
    }

    if (this.draw&&istexting==0&&isshape==0){
        this.ctx.lineTo(e.pageX-this.offsetX,e.pageY-this.offsetY);
        this.ctx.stroke();
    }
}

function mouseUp(e){
    var h;
    var w;
    var hdir = 0;
    var wdir = 0;
    var r;
    var cx;
    var cy;
    var tx1;
    var ty1;
    var tx2;
    var ty2;

    var o=this;
    this.offsetX=this.offsetLeft;
    this.offsetY=this.offsetTop;

    while(o.offsetParent){
        o=o.offsetParent;
        this.offsetX+=o.offsetLeft;
        this.offsetY+=o.offsetTop;
    }

    if(shapey > (e.pageY-this.offsetY)) hdir = 1;
    if(shapex > (e.pageX-this.offsetX)) wdir = 1;

    ty1 = e.pageY-this.offsetY;
    tx1 = e.pageX-this.offsetX;
    
    h = Math.abs(shapey - ty1);
    w = Math.abs(shapex - tx1);
    r = Math.sqrt(h*h + w*w);
    
    ty2 = shapey;
    if(wdir) tx2 = tx1 - w/2;
    else tx2 = tx1 + w/2;
    

    if(isshape==1){
        this.ctx.beginPath();
        this.ctx.arc(shapex,shapey,r,0,2*Math.PI);
        if(isfill) this.ctx.fill();
        else this.ctx.stroke();
    }
    else if(isshape==2){
        this.ctx.beginPath();
        
        if(hdir){
            if(wdir) this.ctx.rect(tx1,ty1,w,h);
            else this.ctx.rect(shapex,ty1,w,h);
        }
        else{
            if(wdir) this.ctx.rect(tx1,shapey,w,h);
            else this.ctx.rect(shapex,shapey,w,h);
        }
        if(isfill) this.ctx.fill();
        else this.ctx.stroke();
    }
    else if(isshape==3){
        this.ctx.beginPath();
        this.ctx.moveTo(shapex,shapey);
        this.ctx.lineTo(tx1,ty1);
        if(!isfill)this.ctx.stroke();
        this.ctx.lineTo(tx2,ty2);
        if(!isfill)this.ctx.stroke();
        this.ctx.moveTo(tx2,ty2);
        this.ctx.lineTo(shapex,shapey);
        if(isfill) this.ctx.fill();
        else this.ctx.stroke();
    }
    this.draw=false;
    cPush();
}

function clearPad(){
    var canvas=document.querySelector('#myCanvas');
    colornow = document.getElementById('colornow');
    var ctx=canvas.getContext("2d");
    ctx.clearRect(0,0,canvas.width,canvas.height);
    ctx.rect(0,0,canvas.width,canvas.height);
    ctx.fillStyle = 'white';
    ctx.fill();
    ctx.fillStyle = 'black';
    cPush();
}

function use(name){
    
    if(name=="erasersetting"){        
        iseraser = 1;
        ctxcolor = 'white';
        color_now(0);
    }
    else document.all[name].style.display="block";  
}

function notuse(name){
    if(name=="erasersetting"){
        if(iseraser)color = lastcolor;
        iseraser = 0;
        colornow = document.getElementById('colornow');
        ctxcolor = colornow.style.backgroundColor;
    }
    else document.all[name].style.display="none"; 
    var canvas = document.getElementById("myCanvas");
    canvas.style.cursor = 'crosshair';
}

function new_width(x){
    width = x;
    widthtext = document.getElementById('widthtext');
    widthtext.textContent = "width = " + width;
}

function new_color(x){
    lastcolor = color;
    if(x==0)color = 'rgb(255,0,0)';
    else if(x==1)color = 'rgb(255,69,0)';
    else if(x==2)color = 'rgb(255,0,140)';
    else if(x==3)color = 'rgb(255,165,0)';
    else if(x==4)color = 'rgb(255,217,0)';
    else if(x==5)color = 'rgb(255, 225,0)';
    else if(x==6)color = 'rgb(208, 255, 0)';
    else if(x==7)color = 'rgb(94, 255, 0)';
    else if(x==8)color = 'rgb(0, 255, 106)';
    else if(x==9)color = 'rgb(0, 255, 213)';
    else if(x==10)color = 'rgb(0, 217, 255)';
    else if(x==11)color = 'rgb(0, 110, 255)';
    else if(x==12)color = 'rgb(47, 0, 255)';
    else if(x==13)color = 'rgb(162, 0, 255)';
    else if(x==14)color = 'rgb(225, 0, 255)';
    else if(x==15)color = 'rgb(255, 0, 221)';
    else if(x==16)color = 'white';
    else color = 'rgb(0,0,0)';
    if(iseraser)ctxcolor = 'white';
    else ctxcolor = color;
    color_now(1);
}

function texting(x){
    if(isshape==1)alert("You are using shape!");
    else istexting = x;
}

function to_img(){
    alert("You are downloading your work!");
}

function settextfont(x){
    if(x==1)textfont = 'Arial';
    else if(x==2)textfont = 'Lobster';
    else textfont = 'Acme';
    document.getElementById("fonttext").textContent = "font = text" + x;
}

function settextsize(x){
    textsize = x;
    if(x==1)document.getElementById("sizetext").textContent = "size = 20px";
    else if(x==2)document.getElementById("sizetext").textContent = "size = 24px";
    else document.getElementById("sizetext").textContent = "size = 28px";
    
}

function new_cursor(x){
    var canvas  = document.getElementById("myCanvas");
    canvas.style.cursor = x;
}

function new_shape(x){
    if(istexting==1)alert("You are using text!");
    else isshape = x;
}

function new_fill(x){
    isfill = x;
}

function cPush(){
    cStep++;
    if(cStep < cPushArray.length){
        cPushArray.length = cStep;
    }
    cPushArray.push(document.getElementById('myCanvas').toDataURL());
}

function cUndo(){
    if(cStep>0){
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { document.getElementById('myCanvas').getContext("2d").drawImage(canvasPic, 0, 0); }
    }
}

function cRedo() {
    if (cStep < cPushArray.length) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { document.getElementById('myCanvas').getContext("2d").drawImage(canvasPic, 0, 0); }
    }
}

var openFile = function(event) {
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function(){
        var dataURL = reader.result;
        var canvasPic = new Image();
        canvasPic.src = dataURL;
        canvasPic.onload = function () { document.getElementById('myCanvas').getContext("2d").drawImage(canvasPic, 0, 0); }
        cPush();
    };
    reader.readAsDataURL(input.files[0]);
};

window.addEventListener('load',function(){
    var canvas = document.querySelector('#myCanvas');
    clearPad();
    canvas.addEventListener('mousedown',mouseDown);
    canvas.addEventListener('mousemove',mouseMove);
    canvas.addEventListener('mouseup',mouseUp);
});
