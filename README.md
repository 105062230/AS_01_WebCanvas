# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed



# 105062230 Report
### 畫布主要使用function

    - global變數shapex、shapey記錄滑鼠在canvas上按下的位置
    - 在window onload時,要先clearPad,幫canvas鋪上畫布底

* **addEventListener()**
    * 宣告變數canvas擷取HTML中的canvas畫布
    * 使用addEventListener來依據滑鼠在畫布上不同狀態觸發不同function(mouseDown()、 mouseMove()、mouseUp()...)

* **mouseDown(e)**
    * 當滑鼠在畫布上按下時觸發的function
    * 使用this.getContext("2d")取得2d畫布環境
    * 使用this.draw代表是否為畫圖狀態,而在這個function內設為true
    * 利用offsetLeft、offsetTop、pageX及pageY相減(canvas位置與滑鼠在瀏覽器頁面上位置相減),來算出滑鼠在canvas上的相對位置
    * 若是正在使用text功能,會擷取HTML中textinput的內容,使用this.getContext("2d").font來改變字型、大小,並且利用fillText()將text繪製在畫布上
    * 將目前滑鼠在canvas上的位置利用global變數shapex及shapey記錄下來,以利在使用shape功能時做為繪製位置參考
    * 利用moveTo()將起始點移至滑鼠的位置

* **mouseMove(e)**
    * 當滑鼠在畫布上移動時觸發的function
    * 同mouseDown(e)方法算出滑鼠在canvas上的相對位置
    * 當this.draw為true,且並非在使用text功能或shape功能時,才會利用lineTo()在canvas上草稿線段以及利用stroke()在canvas上繪製線段

* **mouseUp(e)**
    * 當滑鼠在畫布上放開時觸發的function
    * 同mouseDown(e)方法算出滑鼠在canvas上的相對位置
    * 當使用shape功能時,利用global變數shapex及shapey做為繪製位置參考,使用arc()畫圓、rect()畫方形、lineTo()及moveTo()畫三角形,且利用fill()及stroke()來做出圖形填滿及空心效果
    * 此時繪圖行為結束,呼叫cPush() (Re/On-do功能中的function) 將canvas畫面轉為URL且push進array中紀錄


### Basic Control Tool

    - 變數width記錄要使用的寬度大小
    - 變數color記錄選擇的顏色參數值
    - 變數lastcolor記錄上一個選擇的顏色參數值
    - 變數ctxcolor記錄即將畫在canvas上的顏色參數值
    - 變數iseraser表示eraser功能是否使用中

* **更改線條寬度 new_width(x)**
    * 將width變數改為x

* **更改顏色 new_color(x)**
    * 在改變變數color的參數值前,將color的參數值記錄在參數lastcolor中
    * 依照不同的x更改變數color的參數值

* 若eraser功能使用中,則ctxcolor為白色,反之則為color
* 若eraser功能從使用中切換為關閉,則ctxcolor為colornow.style.backgroundColor(選擇顏色提示區顯示的顏色)


### Text Input

    - 變數istexting表示text功能是否使用中
    - 變數textfont記錄要使用的字型
    - 變數textsize記錄要使用的字體大小

* **使用文字工具 texting(x)**
    * 將變數istexting更新成x
    * 若正在使用shape功能,則不更改istexting,並alert "You are using shape!"
* **更改文字工具字型 settextfont(x)**
    * 依照不同的x更改變數textfont內容
    * 利用textContent更新網頁上提示使用者目前字型的文字內容

* **更改文字工具字體大小 settextsize(x)**
    * 將變數textsize更改為x
    * 利用textContent更新網頁上提示使用者目前字體大小的文字內容


### Cursor icon
* **更改游標 new_cursor(x)**
    * 擷取canvas畫布,更改其style.cursor為x;


### Refresh 功能
* **清空畫布 clearPad()**
    * 使用clearRect()清空一個與畫布相同大小相同的長方形,等於是清空畫布
    * 使用rect()畫一張與畫布相同大小的白色方形作為畫布底


### Different Brush Shape
* **使用圖形工具 new_shape(x)**
    * 將變數isshape更新成x
    * 若正在使用text功能,則不更改istexting,並alert "You are using text!"


### Re/Un-do Buttom

    - 建立Array變數cPushArray,紀錄每一個畫布變更的畫面URL
    - 建立cStep變數,紀錄Array目前畫面index,初始直設為-1,因為push時會++,第一個畫面時cStep才會和cPushArray index一樣是0

* **Push畫布URL進cPushArray cPush()**
    * 即將增加新的畫面,所以cStep加1
    * 利用toDataURL()將畫布目前畫面轉為URL
    * 利用cStep更新cPushArray的長度
    * 若是cPushArray.length大於cStep,代表曾經Redo過,則不更新cPushArray的長度,直接Push覆蓋新的URL進cPushArray

* **Un-do cUndo()**
    * 若是cStep小於等於0,代表目前畫面已是初始畫面,則不做任何事
    * 要拉前一個畫面使用,所以cStep減1
    * 使用變數canvasPic作為Image變數,利用cPushArray中已儲存的畫面URL及更新過的cStep,將它的src設為cPushArray[cStep]
    * 利用drawImage()將上一個畫面canvasPic覆蓋上畫布

* **Re-do cRedo()**
    * 若是cPushArray.length不大於cStep,代表目前已是最新畫面,則不做任何事
    * 要拉下一個畫面使用,所以cStep加1
    * 使用變數canvasPic作為Image變數,利用cPushArray中已儲存的畫面URL及更新過的cStep,將它的src設為cPushArray[cStep]
    * 利用drawImage()將下一個畫面canvasPic覆蓋上畫布


### Image Tool

    - 利用HTML的file type input來選擇上傳之圖片檔案
    <input id="upload_img" style="display:none;" type="file" onchange='openFile(event)'>

* **上傳圖檔 openFile = function(event)**
    * 利用onchange,使得其在當HTML中的upload_img input改變時觸發
    * 使用變數input指向呼叫它的HTML中的upload_img
    * 使用變數reader作為FileReader變數,利用readAsDataURL()讀取上傳圖片的URL
    * 使用變數canvasPic作為Image變數,利用reader讀取上傳圖片後得到的URL設為它的src
    * 利用drawImage()將上傳圖片canvasPic畫上畫布


### Download

    - 利用HTML的download type a 以及canvas畫面的URL來下載canvas上的作品
    <a id="toimg" href="#" download="myCanvas.png" onclick="this.href=document.getElementById('myCanvas').toDataURL()">

* **提示正在下載作品 to_img()**
    * 使用alert()做提示


### Other widgets
* **開啟工具欄 use(id)**
    * 使用all["id"]來擷取物件,並將其display改為block

* **關閉工具欄 unuse(id)**
    * 使用all["id"]來擷取物件,並將其display改為none

* **更改圖形填滿格式 new_fill(x)**
    * 將isfill更改為x


* **更新選擇顏色提示區 color_now(x)**
    * 在選擇顏色時觸發
    * 若正好在使用eraser功能時,使用alert提醒"You are using ERASER now!",因為使用eraser功能時畫筆不會更改顏色,但提示區仍會更新顏色,讓eraser功能關閉後畫筆顏色能成為此顏色
    * 更新選擇顏色提示區方塊顏色及文字顏色

